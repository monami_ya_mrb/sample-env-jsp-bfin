Sample build environment
========================

Target
------
monami-ya.mrb + TOPPERS/JSP for Blackfin

Requirement
-----------
bfin-pizzafactory-elf toolchain (provided by PizzaFactory for Homebrew)

NOTE:
Previous versions of bfin-pizzafactory-elf toolchains has some issues.
So please use recent builds.
I've not checked yet but it will also be built on ADI's gnuchain.

Build
-----

```
git clone git@bitbucket.org:monami_ya_mrb/sample-env-jsp-bfin.git
cd sample-env-jsp-bfin
git submodule update --init
make
```

Artifact
--------

./jsp (stripped binary)
jsp_core/OBJ/jsp (included debugger symbols)

Enjoy!


Professional Support
--------------------
You can get professional supports provided by Monami-ya LLC.
Please contact inquiry@helpdesk.monami-ya.com for more details.
